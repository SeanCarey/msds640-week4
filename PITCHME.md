## Government Regulations    &   
## **DATA PRIVACY**
---
@snap[north span-100]
### **Introduction**
@snapend

@snap[midpoint span-100 fragment]
### *What will we cover?*
@snapend

@snap[south-west span-33 fragment]
@box[text-white rounded span-80](**Privacy** # Legal Aspects     )
@snapend

@snap[south span-33 fragment]
@box[text-white rounded span-80](**Regulation** # The Gramm-Leach-Bliley Act)
@snapend

@snap[south-east span-33 fragment]
@box[text-white rounded span-80](**Compliance** # Corporate Compliance and Enforcement)
@snapend
---

@snap[north span-100]
@box[bg-green text-white rounded span-100](The U.S. Regulatory Model # Two industries and one population with specific laws. )
@snapend

@snap[south-west span-30]
@box[bg-green text-white rounded fragment](Health Care Industry # Health Data)
@snapend

@snap[south-east span-30]
@box[bg-green text-white rounded fragment ](Financial Industry # Financial Data)
@snapend

@snap[south span-30]
@box[bg-blue text-white rounded fragment](Children under 13 # All Data)
@snapend

@snap[midpoint span-100]
@box[ text-white rounded fragment](No Comprehensive Digital Privacy Law)
@snapend

---

@snap[west span-55]
Health Care Data is protected by the Health Information and Portability Accountability Act (HIPAA).
@snapend

@snap[north-east span-45]
![IMAGE](assets/img/health.png)
@snapend

@snap[south span-100]
## Health Data
@snapend

---
@snap[east span-65]
"The Gramm-Leach-Bliley Act requires financial institutions to explain how it collects, shares, and protects customers’ data via a privacy notice that is annually updated."(Ludloff & Craig, 2011)
@snapend

@snap[north-west span-35]
![IMAGE](assets/img/presentation.png)
@snapend

@snap[south span-100]
## Financial Data
@snapend



---
@snap[east span-65]
The Children’s Online Privacy Protection Act gives an explicit privacy policy to children under the age of 13. Websites who collect data must comply. (Ludloff & Craig, 2011)
@snapend

@snap[north-west span-35]
![IMAGE](assets/img/child.png)
@snapend

@snap[south span-100]
## Children
@snapend

---
@snap[south span-100]
### **Gramm-Leach-Bliley Act**
@snapend

@snap[north-west span-30]
@box[text-white rounded fragment](**Established** # 1999)
@snapend

@snap[north span-30]
@box[text-white rounded fragment](**Repealed** # Glass-Steagall Act of 1933)
@snapend

@snap[north-east span-30]
@box[text-white rounded fragment](**Regulates** # Financial Industry)
@snapend



@snap[midpoint span-100]
@box[text-white rounded fragment](**Why was it enacted?** # To modernize and update the financial industry. Before this Commercial Banks were not allowed offer insurance and investment services. This act obligated financial institutions to explain how they shared customer information and allow customers to *opt out*. Also known as GBLM)
@snapend
---

@snap[midpoint span-30]
**Gramm-Leach-Bliley Act Compliance:**
@snapend

@snap[north-west span-30]
@box[text-white rounded fragment](**Safeguard** # Consumer Data)
@snapend

@snap[south-east  span-30]
@box[text-white rounded fragment](**Inform** # Consumers informed of how data is shared.)
@snapend

@snap[north-east  span-30]
@box[text-white rounded fragment](**Document** # Must document an Information Security Plan to protect consumer data.)
@snapend

@snap[south-west span-30]
@box[text-white rounded fragment](**Opt-Out** # Consumers should be able to opt out of sharing data.)
@snapend

---

@snap[midpoint span-30]
**Gramm-Leach-Bliley Act Enforcement:**
@snapend

@snap[north-west span-30]
@box[text-white rounded fragment](**Enforced By:** # Federal Trade Commission)
@snapend

@snap[south-east  span-30]
@box[text-white rounded fragment](**Financial Institutions** # $100,000 fine per violation)
@snapend

@snap[north-east  span-30]
@box[text-white rounded fragment](**Individuals** # $10,000 fine per violation.)
@snapend

@snap[south-west span-30]
@box[text-white rounded fragment](**Individuals** # Up to five years in prison.)
@snapend

---
@box[text-white rounded](**Summary** # It is important to know what kind of data you are responsible for. Different types of data have different regulatory requirements. The United States does not have any all encompassing  Digital Privacy Regulations but you may not always be doing business in the U.S. Research the laws that may be applicable to your specific industry and location. )
